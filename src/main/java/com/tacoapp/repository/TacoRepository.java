package com.tacoapp.repository;


import com.tacoapp.model.Taco;
import org.springframework.data.repository.CrudRepository;

public interface TacoRepository
         extends CrudRepository<Taco, Long> {

}
