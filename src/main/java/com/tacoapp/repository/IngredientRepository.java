package com.tacoapp.repository;

import com.tacoapp.model.Ingredient;
import org.springframework.data.repository.CrudRepository;


public interface IngredientRepository 
         extends CrudRepository<Ingredient, String> {

}
