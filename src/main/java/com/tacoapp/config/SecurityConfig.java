package com.tacoapp.config;

//import com.tacoapp.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder encoder() {
        return new StandardPasswordEncoder("53cr3t");
    }

    //program works with this error!
//    Could not autowire. There is more than one bean of 'UserDetailsService' type.
//            Beans:
//    inMemoryUserDetailsManager   (UserDetailsServiceAutoConfiguration.class) userRepositoryUserDetailsService   (UserRepositoryUserDetailsService.java)
//    less... (Ctrl+F1)
//    Inspection info:Checks autowiring problems in a bean class.
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/design", "/orders")
                .access("hasRole('ROLE_USER')" )
                .antMatchers("/", "/**").permitAll()
        //replace built in login page
        .and()
        .formLogin()
        .loginPage("/login")

        .and()
        .logout()
        .logoutSuccessUrl("/")

        .and()
        .csrf()
        .ignoringAntMatchers("/h2-console/**")

        .and()
        .headers()
        .frameOptions()
        .sameOrigin();


    }


    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(encoder());
    }
}









//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/design", "/orders")
//                .access("hasRole('ROLE_USER')" )
//                .antMatchers("/", "/**").permitAll()
//        //replace built in login page
//        .and()
//        .formLogin()
//        .loginPage("/login")
////                //by default spring security listens on /login and expects username and password field names
////                //custom field names
////        .loginProcessingUrl("/authenticate")
////        .usernameParameter("user")
////        .passwordParameter("pwd");
////                //by default, spring will take you to the page that you were heading too before login interrupt
////                //custom success login page
////        .defaultSuccessUrl("/design");
////            //force page after login
////        .defaultSuccessUrl("/design", true);



//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/design", "/orders")
//                .access("hasRole('ROLE_USER') && " +
//                        "T(java.util.Calendar).getInstance().get(" +
//                        "T(java.util.Calendar).DAY_OF_WEEK) == " +
//                        "T(java.util.Calendar).TUESDAY")
//                .antMatchers("/", "/**").permitAll();
//    }

//        auth
//                .ldapAuthentication()
//                .userSearchBase("ou=people")
//                .userSearchFilter("(uid={0})")
//                .groupSearchBase("ou=groups")
//                .groupSearchFilter("member={0}")
//                .passwordCompare()
//                .passwordEncoder(new BCryptPasswordEncoder())
//                .passwordAttribute("passcode");
//                 .contextSource()
//                //embeded LDAP server
//                .root("dc=tacocloud, dc=com")
//                .ldif("classpath:users.ldif");
        //.url("ldap://tacocloud.com:389/dc=tacocloud, dc=com")
//              auth
//                .jdbcAuthentication()
//                .dataSource(dataSource)
//                .usersByUsernameQuery(
//                        "select username, password, enabled from Users " +
//                                "where username=?")
//                .authoritiesByUsernameQuery(
//                        "select username, authority from UserAuthorities " +
//                                "where username=?")
//                .passwordEncoder(new StackOverflowError("53cr3t"));
//        //.passwordEncoder(new BCryptPasswordEncoder(5));
